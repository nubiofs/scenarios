Scenarios for researching Digital Rails at Av. Paulista
===

The scenarios are named as:
`<traffic-flow>_<signals-planning>_<dr-lanes>_<dr-ratio-%>`

E.g.:
`cross-full-cap_cet_0_0`: Traffic flow restricted to Av. Paulista crossing streets, like R. Augusta, filling all the capacity (in veh/h) provided by OSM. Traffic signals timing plan as defined by CET. 0 lanes dedicated to DR. 0% of vehicles using DR.

`peak_dr_algo_1_75`: Traffic flow for peak hours at Av. Paulista. Traffic signals timing plan as defined by the optimization algorithm (bidirectional green-wave with maximum bandwidth). 1 lane dedicated to DR. 75% os vehicles using DR.